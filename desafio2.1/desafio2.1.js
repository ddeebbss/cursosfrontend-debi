/*
  Una empresa que se dedica del cuidado y mangtenimiento de los bosques, te pide un sofware para calcular la edad de diferentes especies de plantas.
  1. Crear una función que devuelta direrentes funciones para los casos:
  - Si es un pino, la edad será el diámetro (cm) dividido entre dos.
  - Si es un abeto, la edad será la (altura * diámetro)/ 10
  - Si es tomillo, será la altura * 5
  2. Utilizar las funciones creadas para cada caso.
*/

'use strict';
// ********************************************************************
// 1. 
// ********************************************************************



function calcAge(type){
  if (type === 'pino'){
    return function (diametro){ 
    var edadPino = diametro / 2;
      console.log("La edad del Pino es: ", edadPino);
    }
  }
  else if (type === 'abeto'){
    return function (altura,diametro){
    var edadAbeto = (altura * diametro) / 10;
      console.log("La edad del abeto es: ", edadAbeto);
    }
  }
  else if (type === 'tomillo'){
    return function (altura,diametro){
    var edadTomillo=  altura * 5;
      console.log("La edad del tomillo es: ", edadTomillo);
    }
  }
}

var planta=prompt("De que planta (pino/abeto/tomillo) quieres saber la edad");
  if(planta == 'pino'){
    var diametro = parseInt(prompt("Me puedes decir el diametro de por favor?"))
    calcAge(planta)(diametro);
  }else if(planta == 'abeto'){
    var diametro = parseInt(prompt("Me puedes decir el diametro de por favor?"));
    var altura = parseInt(prompt("Me puedes decir el altura de por favor?"));
    calcAge(planta)(diametro, altura);
  }else if(planta == 'tomillo'){
    var altura = parseInt(prompt("Me puedes decir el altura de por favor?"));
    calcAge(planta)(altura);
  }

// HARDCODE
/* var calcAgePino = calcAge('pino');
calcAgePino(100);
esto seria lo mismo de lo de arriba
calcAge('pino')(100);

var calcAgeAbeto = calcAge('abeto');
calcAgeAbeto(50,100);
esto seria lo mismo de lo de arriba
calcAge('abeto')(50,100);

var calcAgeTomillo = calcAge('tomillo');
calcAgeTomillo(50); */
/* esto seria lo mismo de lo de arriba
calcAge('tomillo')(50); */