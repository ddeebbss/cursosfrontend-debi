/*
  En tu empresa te han pedido un software que de soporte al departamento de marketing.
  Se necesita llevar un seguimiento de las redes sociales.
  1. Crear un constructor de objetos socialNetwork, que contengan los siguientes campos: name, arrayLikes, importance (de 0 a 1), numberOfUsers
  2. Usar el contructor para instanciar tres redes sociales:
    - Facebook con likes [201, 245, 500, 650, 1103, 347], importancia 0.8 y 14530 usuarios.
    - Instagram con likes [303, 21, 124, 150, 23, 31], importancia 0.6 y 230 usuarios.
    - Twiter con likes [205, 518, 1123, 4350, 233, 3431], importancia 0.4 y 3230 usuarios.
  3. Añadir 2 métodos en el constructor para calcular el total de likes y la media de cada red social
  4. Cambiar estos dos métodos por dos funciones prototype.
*/



// ******************************************************
// 1.Crear un constructor de objetos socialNetwork
// ******************************************************

var SocialNetwork = function (name, arrayLikes, importance, numberOfUsers){
  this.name = name;
  this.arrayLikes = arrayLikes;
  this.importance = importance;
  this.numberOfUsers = numberOfUsers;
  this.calcTotal = function (){
    this.likes = this.arrayLikes.reduce((a,b) => a + b);
    console.log("El total de likes de " + this.name + " es: ", this.likes);
  } 
  this.calcAverage = function(){
    this.likeAverage = this.likes / this.arrayLikes.length;
    console.log("La media de likes de " + this.name + " es: ", Math.round(this.likeAverage));
  }
}

// ******************************************************
// 2.Usar el contructor para instanciar 
// ******************************************************

var facebook = new SocialNetwork ('Facebook', [201, 245, 500, 650, 1103, 347], 0.8, 14530);
var instagram = new SocialNetwork ('Instagram,', [303, 21, 124, 150, 23, 31], 0.6, 230);
var twiter = new SocialNetwork ('twiter', [205, 518, 1123, 4350, 233, 3431], 0.4, 3230);

console.log("Facebook", facebook);
facebook.calcTotal();
facebook.calcAverage();
console.log("Instagram", instagram);
instagram.calcTotal();
instagram.calcAverage();
console.log("Twiter", twiter);
twiter.calcTotal();
twiter.calcAverage();

// ******************************************************
// 3.calcular el total de likes y la media
// ******************************************************


this.calcTotal = function (){
  this.likes = this.arrayLikes.reduce((a,b) => a + b);
  console.log("El total de likes de " + this.name + " es: ", this.likes);
}  
facebook.calcTotal();



this.calcAverage = function(){
  this.likeAverage = this.likes / this.arrayLikes.length;
  console.log("La media de likes de " + this.name + " es: ", Math.round(this.likeAverage));
}


// *********************************************************
// 4. Cambiar estos dos métodos por dos funciones prototype.
// *********************************************************


/* var SocialNetwork = function (name, arrayLikes, importance, numberOfUsers){
  this.name = name;
  this.arrayLikes = arrayLikes;
  this.importance = importance;
  this.numberOfUsers = numberOfUsers;
}

var facebook = new SocialNetwork('Facebook', [201, 245, 500, 650, 1103, 347], 0.8, 14530);
var instagram = new SocialNetwork('Instagram,', [303, 21, 124, 150, 23, 31], 0.6, 230);
var twiter = new SocialNetwork('twiter', [205, 518, 1123, 4350, 233, 3431], 0.4, 3230);

SocialNetwork.prototype.calcTotal = function (){
  this.likes = this.arrayLikes.reduce((a,b) => a + b);
  console.log("El total de likes de " + this.name + " es: ", this.likes);
}

SocialNetwork.prototype.calcAverage = function (){
  this.likeAverage = this.likes / this.arrayLikes.length;
  console.log("La media de likes de " + this.name + " es: ", Math.round(this.likeAverage));
}

facebook.calcTotal();
facebook.calcAverage();

instagram.calcTotal();
instagram.calcAverage();

twiter.calcTotal();
twiter.calcAverage(); */