'use strict';

// *********************
// Ejercicio map()
// *********************

// Supongamos que tenemos una app que guarda un array de tareas (task) del día. 
// Cada tarea es un objetp con los campos: 'name' y 'duration'
// 1. Queremos crear un array con el nombre de las tareas

// map() es como forEach pero MODIFICA el array original DEVUELVE UN ARRAY 

var task=[
    {
        name:"Programming .map example",
        duration:120
    },
    {
        name:"Programming .filter example",
        duration:20
    },
    {
        name:"Programming .find example",
        duration:30
    },
]


// Forma antigua CON BUCLE
/* var taskNames = [];
for ( i = 0; i < task.length; i++) {
    taskName.push(task[i].name); 
}
console.log("Task name with for: ", taskNames); */


// CON forEach()
/* var taskNames = [];
task.forEach(function(element){
    taskName.push(element.name); 
});
console.log("Task name with for: ", taskNames); */


// CON map()
var taskNames = [];
taskName = task.map(function(element){
    return element.name; 
});
console.log("Task name with for: ", taskNames);
