var luis ={
    name:'Luis' ,
    yearOfBirth: 1987,
    job:'teacher'
  };
  
  var maria ={
    name: 'Maria',
    yearOfBirth: 1990,
    job: 'designer'
  };
  
  //Método constructor
  var Person = function (name, yearOfBirth, job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
  }
  
  // var luis = new Person ('Maria', 1990, 'designer');
  // console.log("Objetos creados CON constructor: ", luis, maria);
  
  
  //Constructor con métodos 
  var Person = function (name, yearOfBirth, job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
    this.calcAge = function (){
      this.age = 2019 - this.yearOfBirth;
      console.log("La edad de " + this.name + " es: ", this.age);
    }  
  }
  
  var luis = new Person ('Luis', 1987, 'teacher');
  luis.calcAge();
  console.log("Creado CON constructor despues de llamar a metodo: ", luis);
  
  //Constructor sin métodos, métodos mediante prototype
  var Person = function (name, yearOfBirth, job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
  }
  
  Person.prototype.calcAge = function (){
    this.age = 2019 - this.yearOfBirth;
    console.log("La edad de " + this.name + " es: ", this.age);
  }
  
  Person.prototype.lastName = 'vicente';
  
  var luis = new Person("Luis",1987, 'teacher');
  console.log("Creado CON constructor y prototype: ", luis);
  
  luis.calcAge();
  console.log("Creado CON constructor y prototype despues de llamar calcAge: ", luis);
  console.log("Apellido obtenido con prototype: ", luis.lastName);