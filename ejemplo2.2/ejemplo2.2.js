// creamos una variable con el metodo de Daate que nos da el año
var currentDate = new Date();
var year = currentDate.getFullYear();

//1. Método antiguao
var luis ={
    name:'Luis' ,
    yearOfBirth: 1987,
    job:'teacher',
    calcAge = function (){
      this.age = year - this.yearOfBirth;
      console.log("La edad de " + this.name + " es: ", this.age);
    }
};


//2. Object.create
var personProto = {
  calcAge : function(){
    console.log("Edad calculada: ",year - this.yearOfBirth)
  }
};

//2.1 Añadir propiedades individualmente
var maria = Object.create(personProto);
maria.name = 'Maria';

//2.2 Añadir propiedades conjuntamente 
var antonio = Object.create(personProto,
  {
    name: { value: 'Antonio'},
    yearOfBirth: {value: 1860},
    job: {value: 'retired'},
  });

console.log("Antonio creado con personProto",antonio);
antonio.calcAge();

