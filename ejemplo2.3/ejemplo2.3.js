'use strict';
// ********************************************************************
// Ejemplo funciones como argumentos
// ********************************************************************

// Array fechas de nacimiento
var year = [1990, 2015, 1937, 2005, 1998];

// Helper function
function calcAge(age){
  return 2019 - age;
}

function isFullAge(age){
  return age >= 18;
}

function maxHeartReate(age){
  if (age >= 18 && age <= 81){
    return Math.round(206.9 - (0.67 * age));
  }
  else{
    return -1;
  }
}

// Main function
function arrayCalc(arr, fn){
  var arrResult = [];
  for (let i = 0; i < arr.length; i++) {
    arrResult.push( fn(arr[i]) );
  }
  return arrResult;
}

// Llamadas
var ages = arrayCalc(year, calcAge);
var fullAges = arrayCalc(ages, isFullAge);
var heartReate = arrayCalc(ages,maxHeartReate);

console.log("Edades: ", ages);
console.log("Mayores de edad: ", fullAges);
console.log("Máximas pulsaciones: ", heartReate);
