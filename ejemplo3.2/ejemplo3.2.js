'use strict';
// ********************************************************************
// Strings en Es6
// ********************************************************************

/* Hasta ahora sumábamos strings con el simnolo +
console.log("Hola" + variable + "que tal estás"); 

En ES6 cambia: se usa el símbolo `${}`
*/

let name = "mundo"
// ES5
console.log("hola "+ name);
// ES6
console.log(`hola ${name} que tal`);
