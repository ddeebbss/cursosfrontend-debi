'use strict';

// ********************************************************************
// Filter, map some, reduce, foreach en ES6
// ********************************************************************

// forEach() se utiliza para el mismo código sobre una array, pero NO MODI el array. Es muy genérico y solo se usa cuando no consigues solucionar un problema con map, some, reduce...

let food = ["mango", "rice", "pepper", "pear"];
food.forEach(function (element){
    console.log(elemnt);
});

// map() es como forEach pero MODIFICA el array original DEVUELVE UN ARRAY 
let money = [200,400,300,1000];
let newMoney = money.map(function(moneyItem){
    return moneyItem / 10;
});
console.log(newMoney);

// filter() comprueba todos los elementos del array en base a en criterio devuelve un array con los elementos que cumplan dicho creiterio
let ages = [18,29,4,67,10];
console.log("Edades: ",ages);

let fullAges = ages.filter(function(itemAge){
    return itemAge >= 18;
});
console.log("Mayores de edad con filter: ",fullAge);

// find() devuelve el valor del primer elemento del array que cumple la función de prueba proporcionada. En cualquier otro caso se devuelve undefined(si no hay ningun elemento que cumpla la condición)
let ages = [18,29,4,67,10];
console.log("Edades: ",ages);

let fullAges2 = ages.filter(function(itemAge){
    return itemAge >= 18;
});
console.log("Mayores de edad con filter: ",fullAge);

// reduce() reducir un array a un único valor. Varias casuísticas

// 1.
var total = [0,1,2,3,4].reduce(function(acumulador, valorActual,indice,array){
    var indice;
    var array;
    return acumulador + valorActual;
})

// 2.
var total = [0,1,2,3,4].reduce(function(a,b){
    return a + b;
});