'use strict';

// *********************
// Arrow function
// *********************

// ES5
function functionName(parameters){
    return parameters;
}

// ES6
var functionNames = (parameters) =>{
    return parameters;
}

console.log("ES5", functionName(5));
console.log("ES6", functionNames(6));

